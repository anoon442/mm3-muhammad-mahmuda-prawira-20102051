<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index()
    {
        return view('login.login');
    }

    public function process(Request $request)
    {
        $credentials = $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);


        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->intended('/students')->with('loginSuccess', 'Login Berhasil');
        }
        return back()->with('loginError', 'Login Gagal');
    }

    public function logout()
    {
        session()->forget('username');
        return redirect('login')->with('pesan', 'Logoutberhasil');
    }
}
