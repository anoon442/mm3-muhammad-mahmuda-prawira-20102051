<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class adminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'username' => 'admin123',
            'password' => bcrypt('admin123'),
        ]);
        DB::table('users')->insert([
            'username' => 'admin123',
            'password' => bcrypt('admin123'),
        ]);
    }
}
