@extends('students.layout')

@section('content')
<div class="row mt-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-start">
            <h2>Laravel 9 CRUD </h2>
        </div>
        <div class="float-end">
            <a class="btn btn-success" href="{{ route('students.create') }}"> Create New student</a>
        </div>
    </div>
</div>

@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Nim</th>
        <th>Name</th>
        <th>JenisKelamin</th>
        <th>Jurusan</th>
        <th>Alamat</th>
        <th width="280px">Action</th>
    </tr>
    @foreach ($students as $student)
    <tr>
        <td>{{ ++$i }}</td>
        <td>{{ $student->nim }}</td>
        <td>{{ $student->name }}</td>
        <td>{{ $student->gender }}</td>
        <td>{{ $student->dapertement }}</td>
        <td>{{ $student->address }}</td>
        <td>
            <form action="{{ route('students.destroy',$student->id) }}" method="POST">

                <a class="btn btn-info" href="{{ route('students.show',$student->id) }}">Show</a>

                <a class="btn btn-primary" href="{{ route('students.edit',$student->id) }}">Edit</a>

                @csrf
                @method('DELETE')

                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>
<div class="row text-center">
    {!! $students->links() !!}
</div>

@endsection