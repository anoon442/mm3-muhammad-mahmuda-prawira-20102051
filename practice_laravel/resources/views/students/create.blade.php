@extends('students.layout')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div>
            <h2>Add New Student</h2>
        </div>
        <div>
            <a class="btn btn-primary" href="{{ route('students.index') }}"> Back</a>
        </div>
    </div>
</div>

@if ($errors->any())
<div class="alert alert-danger">
    <strong>Whoops!</strong> There were some problems with your input.<br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form action="{{ route('students.store') }}" method="POST">
    @csrf

    <div class="row">
        <div class="form-group">
            <label for="nim">NIM</label>
            <input type="text" class="form-control @error('nim') is-invalid @enderror" id="nim" name="nim"
                value="{{old('nim')}}">

            @error('nim')
            <div class="text-danger">
                {{$message}}
            </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="name">NamaLengkap</label>
            <input type="text" class="form-control @error('name')is-invalid @enderror" id="name" name="name"
                value="{{old('nama')}}">
            @error('nama')
            <div class="text-danger">
                {{$message}}
            </div>
            @enderror
        </div>
        <div class="form-group"><label>Jenis Kelamin</label>
            <div>
                <div class="form-checkform-check-inline">
                    <input class="form-check-input" type="radio" name="gender" id="laki_laki" value="L"
                        {{old('gender')=='L' ?'checked':''}}>
                    <label class="form-check-label" for="laki_laki">Laki-laki</label>
                </div>
                <div class="form-checkform-check-inline">
                    <input class="form-check-input" type="radio" name="gender" id="perempuan" value="P"
                        {{old('gender')=='P' ?'checked':''}}>
                    <label class="form-check-label" for="perempuan">Perempuan</label>
                </div>
                @error('gender')
                <div class="text-danger">
                    {{$message}}
                </div>@enderror
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <div class="form-group">
                <label for="dapertement">Jurusan</label>
                <select class="form-control" name="dapertement" id="dapertement">
                    <option value="Teknik Informatika" {{old('dapertement')=='Teknik Informatika' ?'selected':''}}>
                        Teknik Informatika</option>
                    <option value="Sistem Informasi" {{old('dapertement')=='Sistem Informasi' ?'selected':''}}>Sistem
                        Informasi</option>
                    <option value="Ilmu Komputer" {{old('dapertement')=='Ilmu Komputer' ?'selected':''}}>Ilmu Komputer
                    </option>
                    <option value="Teknik Komputer" {{old('dapertement')=='Teknik Komputer' ?'selected':''}}>Teknik
                        Komputer
                    </option>
                    <option value="Teknik Telekomunikasi" {{old('dapertement')=='Teknik Telekomunikasi'
                        ?'selected':''}}>
                        Teknik Telekomunikasi</option>
                </select>
                @error('jurusan')
                <div class="text-danger">
                    {{$message}}
                </div>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for="address">Alamat</label>
            <textarea class="form-control" id="address" rows="3" name="address">{{old('alamat')}}</textarea>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center mt-3">
            <button type="submit" class="btn btn-success">Submit</button>
        </div>
    </div>
</form>
@endsection